package dao;

import model.User;

/**
 * Created by moro on 1/18/17.
 */
public class TestUserDAO {

    public static void main(String[] args) {
        UserDAO dao = new UserDAO();
        System.out.println("inserting user \"test-user\"");
        User u = new User("test-user", "pass", "user");
        dao.addUser(u);
        User test = dao.findUser("test-user");
        System.out.println("Found user " + test.getUsername());
    }
}
