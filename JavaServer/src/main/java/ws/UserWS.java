package ws;

import model.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by moro on 1/18/17.
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserWS {

    @WebMethod
    String getUserType(String username);
    @WebMethod String addUser(User user);
    @WebMethod User findUser(String username);

}