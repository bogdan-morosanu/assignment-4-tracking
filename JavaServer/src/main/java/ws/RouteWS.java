package ws;

import model.Route;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by moro on 1/18/17.
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface RouteWS {
    @WebMethod
    Route[] getRoutes(String cityFrom);
    @WebMethod Route addRoute(Route route);
}
