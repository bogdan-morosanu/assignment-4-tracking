package ws;

import dao.UserDAO;
import model.User;

import javax.jws.WebService;

/**
 * Created by moro on 1/18/17.
 */

@WebService(endpointInterface = "ws.UserWS")
public class ImplUserWS implements UserWS {

    public String getUserType(String username){
        UserDAO userDAO = new UserDAO();
        User u = userDAO.findUser(username);

        if (u == null){
            return "missing";
        }
        else{
            return u.getType();
        }
    }

    public User findUser(String username){
        UserDAO userDAO = new UserDAO();
        User u = userDAO.findUser(username);
        if (u == null){
            User dummy = new User();
            dummy.setUsername("missing");
            dummy.setPassword("missing");
            dummy.setType("missing");
            return dummy;
        }
        return u;
    }

    public String addUser(User user) {
        UserDAO userDAO = new UserDAO();
        User u = userDAO.addUser(user);
        return u.getUsername();
    }

}
