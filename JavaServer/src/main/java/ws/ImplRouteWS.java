package ws;

import dao.RouteDAO;
import model.Route;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by moro on 1/18/17.
 */

@WebService(endpointInterface = "ws.RouteWS")
public class ImplRouteWS implements RouteWS {

    public Route[] getRoutes(String cityFrom) {
        RouteDAO routeDAO = new RouteDAO();
        List<Route> res = routeDAO.findRoutes();
        List<Route> tosend = new ArrayList<Route>();
        for (Route r : res){
            if (new Integer(r.getPackageId()).toString().equals(cityFrom)) {
                tosend.add(r);
            }
        }
        return tosend.toArray(new Route[tosend.size()]);
    }

    public Route addRoute(Route route) {
        RouteDAO routeDAO = new RouteDAO();
        routeDAO.addRoute(route);
        return route;
    }
}
