package ws;

import dao.PacketDAO;
import model.Packet;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by moro on 1/18/17.
 */

@WebService(endpointInterface = "ws.PacketWS")
public class ImplPacketWS implements PacketWS {

    public Packet[] getPackets() {
        PacketDAO packetDAO = new PacketDAO();
        List<Packet> result= packetDAO.findPackets();
        return result.toArray(new Packet[result.size()]);
    }

    public Packet[] getPacketsByUser(String username) {
        PacketDAO packetDAO = new PacketDAO();
        List<Packet> result= packetDAO.findPackets();
        List<Packet> tosend = new ArrayList<Packet>();
        for (Packet p: result){
            if (p.getReceiver().equals(username) || p.getSender().equals(username)){
                tosend.add(p);
            }
        }
        return tosend.toArray(new Packet[tosend.size()]);
    }

    public Packet addPacket(Packet packet) {
        PacketDAO packetDAO = new PacketDAO();
        packetDAO.addPacket(packet);
        return packet;
    }

    public String deletePacket(int id) {
        PacketDAO packetDAO = new PacketDAO();
        Packet res = packetDAO.deletePacket(id);
        if (res == null){
            return "notfound";
        }
        else{
            return "deleted";
        }
    }

    public Packet findPacket(int id) {
        PacketDAO packetDAO = new PacketDAO();
        return packetDAO.findPacket(id);
    }

    public String updateTracking(int id, boolean tracking) {
        PacketDAO packetDAO = new PacketDAO();
        Packet p = packetDAO.findPacket(id);
        if (p != null){
            p.setTracked(tracking);
            packetDAO.updatePacket(p);
            return "updated";
        }
        else{
            return "notfound";
        }
    }
}