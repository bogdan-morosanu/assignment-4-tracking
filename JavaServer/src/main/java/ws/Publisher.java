package ws;

import javax.xml.ws.Endpoint;

/**
 * Created by moro on 1/18/17.
 */

public class Publisher {
    public static void main(String[] args){
        Endpoint.publish("http://localhost:9000/user", new ImplUserWS());
        Endpoint.publish("http://localhost:9000/packet", new ImplPacketWS());
        Endpoint.publish("http://localhost:9000/route", new ImplRouteWS());

    }
}
