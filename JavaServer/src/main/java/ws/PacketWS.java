package ws;

import model.Packet;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Created by moro on 1/18/17.
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface PacketWS {

    @WebMethod
    Packet[] getPackets();
    @WebMethod Packet[] getPacketsByUser(String username);
    @WebMethod Packet addPacket(Packet packet);
    @WebMethod String deletePacket(int id);
    @WebMethod Packet findPacket(int id);
    @WebMethod String updateTracking(int id, boolean tracking);

}