package model;

/**
 * Created by moro on 1/17/17.
 */

public class Route {

    public Route() { }

    public Route(int packageId, String cityTo, String time) {
        this.packageId = packageId;
        this.cityTo = cityTo;
        this.time = time;

    }

    private int id;
    private int packageId;
    private String cityTo;
    private String time;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }


    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getCityTo() {

        return cityTo;
    }

    public void setCityTo(String cityTo) {

        this.cityTo = cityTo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
