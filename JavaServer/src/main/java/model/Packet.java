package model;

/**
 * Created by moro on 1/17/17.
 */

public class Packet {
    private int id;
    private String sender;
    private String receiver;
    private String name;
    private String description;
    private String cityFrom;
    private String cityTo;
    private boolean tracked;

    public Packet() {   }

    public Packet(String cityFrom, String cityTo, String description,
                  String name, String sender, String receiver) {
        this.cityFrom = cityFrom;
        this.cityTo = cityTo;
        this.name = name;
        this.description = description;
        this.sender = sender;
        this.receiver = receiver;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public boolean isTracked() {
        return tracked;
    }

    public void setTracked(boolean tracked) {
        this.tracked = tracked;
    }
}
