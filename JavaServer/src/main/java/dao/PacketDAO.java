package dao;

import model.Packet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateHelper;

import java.util.List;

/**
 * Created by moro on 1/17/17.
 */

public class PacketDAO {
    private static final Log LOGGER = LogFactory.getLog(UserDAO.class);

    public Packet findPacket(int id) {
        Session session = HibernateHelper.openSession();
        Transaction tx = null;
        List<Packet> packets = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Packet WHERE id = :id");
            query.setParameter("id", id);
            packets = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return packets != null && !packets.isEmpty() ? packets.get(0) : null;
    }

    public Packet addPacket(Packet packet) {
        Session session = HibernateHelper.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(packet);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return packet;
    }

    public Packet deletePacket(int id) {
        Session session = HibernateHelper.openSession();
        Transaction tx = null;
        List<Packet> packets = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Packet WHERE id = :id");
            query.setParameter("id", id);
            packets = query.list();
            if (packets.size() != 0) {
                session.delete(packets.get(0));
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return packets != null && !packets.isEmpty() ? packets.get(0) : null;
    }

    public Packet updatePacket(Packet packet){
        Session session = HibernateHelper.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(packet);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return packet;
    }

    public List<Packet> findPackets() {
        Session session = HibernateHelper.openSession();
        Transaction tx = null;
        List<Packet> packets = null;
        try {
            tx = session.beginTransaction();
            packets = session.createQuery("FROM Packet").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return packets;
    }
}
