package dao;

import model.Route;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateHelper;

import java.util.List;

/**
 * Created by moro on 1/17/17.
 */

public class RouteDAO {
    private static final Log LOGGER = LogFactory.getLog(UserDAO.class);

    public Route addRoute(Route route) {
        Session session = HibernateHelper.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(route);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return route;
    }

    public List<Route> findRoutes() {
        Session session = HibernateHelper.openSession();
        Transaction tx = null;
        List<Route> routes = null;
        try {
            tx = session.beginTransaction();
            routes = session.createQuery("FROM Route").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return routes;
    }
}