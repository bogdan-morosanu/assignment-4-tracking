#include <stdlib.h>
#include <iostream>

// MySql Includes
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

// Soap Includes
#include "soap/soapH.h"
#include "soap/soapStub.h"
#include "soap/ImplPacketWSPortBinding.nsmap"
#include "soap/soapImplPacketWSPortBindingService.h"

static const std::string QUERRY = "SELECT * FROM package";

using namespace std;

int main(void)
{

  ImplPacketWSPortBindingService user_service(SOAP_XML_INDENT);
  if (user_service.run(9000) != SOAP_OK) {
    user_service.soap_stream_fault(std::cerr);
  }
  
  user_service.destroy(); // same as: soap_destroy(calc.soap); soap_end(calc.soap);
  
  return EXIT_SUCCESS;
}

/// Web service operation 'getPackets' (returns SOAP_OK or error code)
int ImplPacketWSPortBindingService::getPackets(struct ns1__getPacketsResponse &_param_1)
{
  return SOAP_OK;
}

/// Web service operation 'getPacketsByUser' (returns SOAP_OK or error code)
int ImplPacketWSPortBindingService::getPacketsByUser(std::string arg0, struct ns1__getPacketsByUserResponse &_param_1)
{
  std::string q = "SELECT *  FROM package WHERE package.receiver = " + arg0 + ";";
  
  std::cout << q << std::endl;

  try {
    sql::Driver *driver;
    sql::Connection *con;
    sql::Statement *stmt;
    sql::ResultSet *res;

    /* Create a connection */
    driver = get_driver_instance();
    con = driver->connect("tcp://127.0.0.1:3306", "root", "dummy-pass");
    /* Connect to the MySQL test database */
    con->setSchema("assignment-four-db");

    stmt = con->createStatement();
    res = stmt->executeQuery(q.c_str()); 

    while (res->next()) {
      cout << "\t... MySQL replies: ";
      /* Access column fata by numeric offset, 1 is the first column */
      for (int i = 1; i < 8; ++i)
	cout << res->getString(i) << " ";

      cout << endl;
    }
    
    delete res;
    delete stmt;
    delete con;

  } catch (sql::SQLException &e) {
    cout << "# ERR: SQLException in " << __FILE__;
    cout << "(" << __FUNCTION__ << ") on line "
	 << __LINE__ << endl;
    cout << "# ERR: " << e.what();
    cout << " (MySQL error code: " << e.getErrorCode();
    cout << ", SQLState: " << e.getSQLState() << " )" << endl;
  }

  std::cout << endl;
  
  return SOAP_OK;
}

/// Web service operation 'addPacket' (returns SOAP_OK or error code)
int ImplPacketWSPortBindingService::addPacket(ns1__packet *arg0, struct ns1__addPacketResponse &_param_1)
{
  return SOAP_OK;
}

/// Web service operation 'deletePacket' (returns SOAP_OK or error code)
int ImplPacketWSPortBindingService::deletePacket(int arg0, struct ns1__deletePacketResponse &_param_1)
{
  return SOAP_OK;
}

/// Web service operation 'findPacket' (returns SOAP_OK or error code)
int ImplPacketWSPortBindingService::findPacket(int arg0, struct ns1__findPacketResponse &_param_1)
{
  return SOAP_OK;
}

/// Web service operation 'updateTracking' (returns SOAP_OK or error code)
int ImplPacketWSPortBindingService::updateTracking(int _arg0, bool _arg1, struct ns1__updateTrackingResponse &_param_1)
{
  return SOAP_OK;
}
