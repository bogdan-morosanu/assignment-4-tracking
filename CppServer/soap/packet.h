// Reminder: Modify typemap.dat to customize the header file generated by wsdl2h
/* packet.h
   Generated by wsdl2h 2.8.40 from http://localhost:9000/packet?wsdl and typemap.dat
   2017-01-18 04:53:20 GMT

   DO NOT INCLUDE THIS FILE DIRECTLY INTO YOUR PROJECT BUILDS
   USE THE soapcpp2-GENERATED SOURCE CODE FILES FOR YOUR PROJECT BUILDS

gSOAP XML Web services tools
Copyright (C) 2000-2016, Robert van Engelen, Genivia Inc. All Rights Reserved.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
--------------------------------------------------------------------------------
A commercial use license is available from Genivia Inc., contact@genivia.com
--------------------------------------------------------------------------------
*/

/** @page page_notes Notes

@note HINTS:
 - Run soapcpp2 on packet.h to generate the SOAP/XML processing logic.
   Use soapcpp2 -I to specify paths for #import
   To build with STL, 'stl.h' is imported from 'import' dir in package.
   Use soapcpp2 -j to generate improved proxy and server classes.
   Use soapcpp2 -r to generate a report.
 - Use wsdl2h -c and -s to generate pure C code or C++ code without STL.
 - Use 'typemap.dat' to control namespace bindings and type mappings.
   It is strongly recommended to customize the names of the namespace prefixes
   generated by wsdl2h. To do so, modify the prefix bindings in the Namespaces
   section below and add the modified lines to 'typemap.dat' to rerun wsdl2h.
 - Run Doxygen (www.doxygen.org) on this file to generate documentation.
 - Use wsdl2h -R to generate REST operations.
 - Use wsdl2h -nname to use name as the base namespace prefix instead of 'ns'.
 - Use wsdl2h -Nname for service prefix and produce multiple service bindings
 - Use wsdl2h -d to enable DOM support for xsd:anyType.
 - Use wsdl2h -g to auto-generate readers and writers for root elements.
 - Use wsdl2h -b to auto-generate bi-directional operations (duplex ops).
 - Use wsdl2h -U to map XML names to C++ Unicode identifiers instead of _xNNNN.
 - Use wsdl2h -u to disable the generation of unions.
 - Struct/class members serialized as XML attributes are annotated with a '@'.
 - Struct/class members that have a special role are annotated with a '$'.

@warning
   DO NOT INCLUDE THIS ANNOTATED FILE DIRECTLY IN YOUR PROJECT SOURCE CODE.
   USE THE FILES GENERATED BY soapcpp2 FOR YOUR PROJECT'S SOURCE CODE:
   THE soapStub.h FILE CONTAINS THIS CONTENT WITHOUT ANNOTATIONS.

@copyright LICENSE:
@verbatim
--------------------------------------------------------------------------------
gSOAP XML Web services tools
Copyright (C) 2000-2016, Robert van Engelen, Genivia Inc. All Rights Reserved.
The wsdl2h tool and its generated software are released under the GPL.
This software is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
--------------------------------------------------------------------------------
GPL license.

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place, Suite 330, Boston, MA 02111-1307 USA

Author contact information:
engelen@genivia.com / engelen@acm.org

This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
--------------------------------------------------------------------------------
A commercial-use license is available from Genivia, Inc., contact@genivia.com
--------------------------------------------------------------------------------
@endverbatim

*/


//gsoapopt c++,w

/******************************************************************************\
 *                                                                            *
 * Definitions                                                                *
 *   http://ws/                                                               *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * $CONTAINER                                                                 *
 *   std::vector                                                              *
 *                                                                            *
\******************************************************************************/

template <class T> class std::vector;

/******************************************************************************\
 *                                                                            *
 * Import                                                                     *
 *                                                                            *
\******************************************************************************/

#import "stl.h"	// enable STL containers when used (option -s removes STL dependency)

/******************************************************************************\
 *                                                                            *
 * Schema Namespaces                                                          *
 *                                                                            *
\******************************************************************************/


/* NOTE:

It is strongly recommended to customize the names of the namespace prefixes
generated by wsdl2h. To do so, modify the prefix bindings below and add the
modified lines to typemap.dat to rerun wsdl2h:

ns1 = "http://ws/"

*/

#define SOAP_NAMESPACE_OF_ns1	"http://ws/"
//gsoap ns1   schema namespace:	http://ws/
//gsoap ns1   schema form:	unqualified

/******************************************************************************\
 *                                                                            *
 * Built-in Schema Types and Top-Level Elements and Attributes                *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Forward Declarations                                                       *
 *                                                                            *
\******************************************************************************/


//  Forward declaration of class ns1__packet.
class ns1__packet;

//  Forward declaration of class ns1__packetArray.
class ns1__packetArray;


/******************************************************************************\
 *                                                                            *
 * Schema Types and Top-Level Elements and Attributes                         *
 *                                                                            *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Schema Types and Top-Level Elements and Attributes                         *
 *   http://ws/                                                               *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Schema Complex Types and Top-Level Elements                                *
 *                                                                            *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Schema Complex Types and Top-Level Elements                                *
 *   http://ws/                                                               *
 *                                                                            *
\******************************************************************************/

/// @brief "http://ws/":packet is a complexType.
///
/// class ns1__packet operations:
/// - ns1__packet* soap_new_ns1__packet(soap*) allocate and default initialize
/// - ns1__packet* soap_new_ns1__packet(soap*, int num) allocate array and default initialize values
/// - ns1__packet* soap_new_req_ns1__packet(soap*, ...) allocate, set required members
/// - ns1__packet* soap_new_set_ns1__packet(soap*, ...) allocate, set all public members
/// - ns1__packet::soap_default(soap*) default initialize members
/// - int soap_read_ns1__packet(soap*, ns1__packet*) deserialize from a stream
/// - int soap_write_ns1__packet(soap*, ns1__packet*) serialize to a stream
/// - ns1__packet* ns1__packet::soap_dup(soap*) returns deep copy of ns1__packet, copies the (cyclic) graph structure when a context is provided, or (cycle-pruned) tree structure with soap_set_mode(soap, SOAP_XML_TREE) (use soapcpp2 -Ec)
/// - ns1__packet::soap_del() deep deletes ns1__packet data members, use only after ns1__packet::soap_dup(NULL) (use soapcpp2 -Ed)
class ns1__packet
{ public:
/// Element "cityFrom" of XSD type xs:string.
    std::string*                         cityFrom                       0;	///< Optional element.
/// Element "cityTo" of XSD type xs:string.
    std::string*                         cityTo                         0;	///< Optional element.
/// Element "description" of XSD type xs:string.
    std::string*                         description                    0;	///< Optional element.
/// Element "id" of XSD type xs:int.
    int                                  id                             1;	///< Required element.
/// Element "name" of XSD type xs:string.
    std::string*                         name                           0;	///< Optional element.
/// Element "receiver" of XSD type xs:string.
    std::string*                         receiver                       0;	///< Optional element.
/// Element "sender" of XSD type xs:string.
    std::string*                         sender                         0;	///< Optional element.
/// Element "tracked" of XSD type xs:boolean.
    bool                                 tracked                        1;	///< Required element.
/// A handle to the soap struct context that manages this instance when instantiated by a context or NULL otherwise (automatically set).
    struct soap                         *soap                          ;
};

/// @brief "http://ws/":packetArray is a complexType.
///
/// class ns1__packetArray operations:
/// - ns1__packetArray* soap_new_ns1__packetArray(soap*) allocate and default initialize
/// - ns1__packetArray* soap_new_ns1__packetArray(soap*, int num) allocate array and default initialize values
/// - ns1__packetArray* soap_new_req_ns1__packetArray(soap*, ...) allocate, set required members
/// - ns1__packetArray* soap_new_set_ns1__packetArray(soap*, ...) allocate, set all public members
/// - ns1__packetArray::soap_default(soap*) default initialize members
/// - int soap_read_ns1__packetArray(soap*, ns1__packetArray*) deserialize from a stream
/// - int soap_write_ns1__packetArray(soap*, ns1__packetArray*) serialize to a stream
/// - ns1__packetArray* ns1__packetArray::soap_dup(soap*) returns deep copy of ns1__packetArray, copies the (cyclic) graph structure when a context is provided, or (cycle-pruned) tree structure with soap_set_mode(soap, SOAP_XML_TREE) (use soapcpp2 -Ec)
/// - ns1__packetArray::soap_del() deep deletes ns1__packetArray data members, use only after ns1__packetArray::soap_dup(NULL) (use soapcpp2 -Ed)
class ns1__packetArray
{ public:
/// Vector of ns1__packet* of length 0..unbounded.
    std::vector<ns1__packet*           > item                           0;
/// A handle to the soap struct context that manages this instance when instantiated by a context or NULL otherwise (automatically set).
    struct soap                         *soap                          ;
};


/******************************************************************************\
 *                                                                            *
 * Additional Top-Level Elements                                              *
 *                                                                            *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Additional Top-Level Attributes                                            *
 *                                                                            *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Additional Top-Level Elements                                              *
 *   http://ws/                                                               *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Additional Top-Level Attributes                                            *
 *   http://ws/                                                               *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Services                                                                   *
 *                                                                            *
\******************************************************************************/


//gsoap ns1  service name:	ImplPacketWSPortBinding 
//gsoap ns1  service type:	PacketWS 
//gsoap ns1  service port:	http://localhost:9000/packet 
//gsoap ns1  service namespace:	http://ws/ 
//gsoap ns1  service transport:	http://schemas.xmlsoap.org/soap/http 

/** @mainpage ImplPacketWSService Definitions

@section ImplPacketWSService_bindings Service Bindings

  - @ref ImplPacketWSPortBinding

@section ImplPacketWSService_more More Information

  - @ref page_notes "Notes"

  - @ref page_XMLDataBinding "XML Data Binding"

  - @ref SOAP_ENV__Header "SOAP Header Content" (when applicable)

  - @ref SOAP_ENV__Detail "SOAP Fault Detail Content" (when applicable)


*/

/**

@page ImplPacketWSPortBinding Binding "ImplPacketWSPortBinding"

@section ImplPacketWSPortBinding_operations Operations of Binding "ImplPacketWSPortBinding"

  - @ref ns1__getPackets

  - @ref ns1__getPacketsByUser

  - @ref ns1__addPacket

  - @ref ns1__deletePacket

  - @ref ns1__findPacket

  - @ref ns1__updateTracking

@section ImplPacketWSPortBinding_ports Default endpoints of Binding "ImplPacketWSPortBinding"

  - http://localhost:9000/packet

@note Use wsdl2h option -Nname to change the service binding prefix name


*/

/******************************************************************************\
 *                                                                            *
 * Service Binding                                                            *
 *   ImplPacketWSPortBinding                                                  *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Service Operation                                                          *
 *   ns1__getPackets                                                          *
 *                                                                            *
\******************************************************************************/

/// Operation response struct "ns1__getPacketsResponse" of operation "ns1__getPackets".
struct ns1__getPacketsResponse
{
    ns1__packetArray*                   return_;	///< Output parameter
};

/** Operation "ns1__getPackets" of service binding "ImplPacketWSPortBinding".

  - SOAP literal messaging

  - Default endpoints:
    - http://localhost:9000/packet

  - Addressing input action: "http://ws/PacketWS/getPacketsRequest"

  - Addressing output action: "http://ws/PacketWS/getPacketsResponse"

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call_ns1__getPackets(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // input parameters:
    // output parameters:
    struct ns1__getPacketsResponse&
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int ns1__getPackets(
    struct soap *soap,
    // input parameters:
    // output parameters:
    struct ns1__getPacketsResponse&
  );
@endcode

C++ proxy class (defined in soapImplPacketWSPortBindingProxy.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingProxy;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use proxy classes;

C++ service class (defined in soapImplPacketWSPortBindingService.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingService;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use service classes;

*/

//gsoap ns1  service method-protocol:	getPackets SOAP
//gsoap ns1  service method-style:	getPackets rpc
//gsoap ns1  service method-encoding:	getPackets literal
//gsoap ns1  service method-input-action:	getPackets http://ws/PacketWS/getPacketsRequest
//gsoap ns1  service method-output-action:	getPackets http://ws/PacketWS/getPacketsResponse
int ns1__getPackets(
    struct ns1__getPacketsResponse     &	///< Output response struct parameter
);

/******************************************************************************\
 *                                                                            *
 * Service Operation                                                          *
 *   ns1__getPacketsByUser                                                    *
 *                                                                            *
\******************************************************************************/

/// Operation response struct "ns1__getPacketsByUserResponse" of operation "ns1__getPacketsByUser".
struct ns1__getPacketsByUserResponse
{
    ns1__packetArray*                   return_;	///< Output parameter
};

/** Operation "ns1__getPacketsByUser" of service binding "ImplPacketWSPortBinding".

  - SOAP literal messaging

  - Default endpoints:
    - http://localhost:9000/packet

  - Addressing input action: "http://ws/PacketWS/getPacketsByUserRequest"

  - Addressing output action: "http://ws/PacketWS/getPacketsByUserResponse"

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call_ns1__getPacketsByUser(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // input parameters:
    std::string                         arg0,
    // output parameters:
    struct ns1__getPacketsByUserResponse&
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int ns1__getPacketsByUser(
    struct soap *soap,
    // input parameters:
    std::string                         arg0,
    // output parameters:
    struct ns1__getPacketsByUserResponse&
  );
@endcode

C++ proxy class (defined in soapImplPacketWSPortBindingProxy.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingProxy;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use proxy classes;

C++ service class (defined in soapImplPacketWSPortBindingService.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingService;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use service classes;

*/

//gsoap ns1  service method-protocol:	getPacketsByUser SOAP
//gsoap ns1  service method-style:	getPacketsByUser rpc
//gsoap ns1  service method-encoding:	getPacketsByUser literal
//gsoap ns1  service method-input-action:	getPacketsByUser http://ws/PacketWS/getPacketsByUserRequest
//gsoap ns1  service method-output-action:	getPacketsByUser http://ws/PacketWS/getPacketsByUserResponse
int ns1__getPacketsByUser(
    std::string                         arg0,	///< Input parameter
    struct ns1__getPacketsByUserResponse&	///< Output response struct parameter
);

/******************************************************************************\
 *                                                                            *
 * Service Operation                                                          *
 *   ns1__addPacket                                                           *
 *                                                                            *
\******************************************************************************/

/// Operation response struct "ns1__addPacketResponse" of operation "ns1__addPacket".
struct ns1__addPacketResponse
{
    ns1__packet*                        return_;	///< Output parameter
};

/** Operation "ns1__addPacket" of service binding "ImplPacketWSPortBinding".

  - SOAP literal messaging

  - Default endpoints:
    - http://localhost:9000/packet

  - Addressing input action: "http://ws/PacketWS/addPacketRequest"

  - Addressing output action: "http://ws/PacketWS/addPacketResponse"

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call_ns1__addPacket(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // input parameters:
    ns1__packet*                        arg0,
    // output parameters:
    struct ns1__addPacketResponse&
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int ns1__addPacket(
    struct soap *soap,
    // input parameters:
    ns1__packet*                        arg0,
    // output parameters:
    struct ns1__addPacketResponse&
  );
@endcode

C++ proxy class (defined in soapImplPacketWSPortBindingProxy.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingProxy;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use proxy classes;

C++ service class (defined in soapImplPacketWSPortBindingService.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingService;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use service classes;

*/

//gsoap ns1  service method-protocol:	addPacket SOAP
//gsoap ns1  service method-style:	addPacket rpc
//gsoap ns1  service method-encoding:	addPacket literal
//gsoap ns1  service method-input-action:	addPacket http://ws/PacketWS/addPacketRequest
//gsoap ns1  service method-output-action:	addPacket http://ws/PacketWS/addPacketResponse
int ns1__addPacket(
    ns1__packet*                        arg0,	///< Input parameter
    struct ns1__addPacketResponse      &	///< Output response struct parameter
);

/******************************************************************************\
 *                                                                            *
 * Service Operation                                                          *
 *   ns1__deletePacket                                                        *
 *                                                                            *
\******************************************************************************/

/// Operation response struct "ns1__deletePacketResponse" of operation "ns1__deletePacket".
struct ns1__deletePacketResponse
{
    std::string                         return_;	///< Output parameter
};

/** Operation "ns1__deletePacket" of service binding "ImplPacketWSPortBinding".

  - SOAP literal messaging

  - Default endpoints:
    - http://localhost:9000/packet

  - Addressing input action: "http://ws/PacketWS/deletePacketRequest"

  - Addressing output action: "http://ws/PacketWS/deletePacketResponse"

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call_ns1__deletePacket(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // input parameters:
    int                                 arg0,
    // output parameters:
    struct ns1__deletePacketResponse&
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int ns1__deletePacket(
    struct soap *soap,
    // input parameters:
    int                                 arg0,
    // output parameters:
    struct ns1__deletePacketResponse&
  );
@endcode

C++ proxy class (defined in soapImplPacketWSPortBindingProxy.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingProxy;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use proxy classes;

C++ service class (defined in soapImplPacketWSPortBindingService.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingService;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use service classes;

*/

//gsoap ns1  service method-protocol:	deletePacket SOAP
//gsoap ns1  service method-style:	deletePacket rpc
//gsoap ns1  service method-encoding:	deletePacket literal
//gsoap ns1  service method-input-action:	deletePacket http://ws/PacketWS/deletePacketRequest
//gsoap ns1  service method-output-action:	deletePacket http://ws/PacketWS/deletePacketResponse
int ns1__deletePacket(
    int                                 arg0,	///< Input parameter
    struct ns1__deletePacketResponse   &	///< Output response struct parameter
);

/******************************************************************************\
 *                                                                            *
 * Service Operation                                                          *
 *   ns1__findPacket                                                          *
 *                                                                            *
\******************************************************************************/

/// Operation response struct "ns1__findPacketResponse" of operation "ns1__findPacket".
struct ns1__findPacketResponse
{
    ns1__packet*                        return_;	///< Output parameter
};

/** Operation "ns1__findPacket" of service binding "ImplPacketWSPortBinding".

  - SOAP literal messaging

  - Default endpoints:
    - http://localhost:9000/packet

  - Addressing input action: "http://ws/PacketWS/findPacketRequest"

  - Addressing output action: "http://ws/PacketWS/findPacketResponse"

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call_ns1__findPacket(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // input parameters:
    int                                 arg0,
    // output parameters:
    struct ns1__findPacketResponse&
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int ns1__findPacket(
    struct soap *soap,
    // input parameters:
    int                                 arg0,
    // output parameters:
    struct ns1__findPacketResponse&
  );
@endcode

C++ proxy class (defined in soapImplPacketWSPortBindingProxy.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingProxy;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use proxy classes;

C++ service class (defined in soapImplPacketWSPortBindingService.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingService;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use service classes;

*/

//gsoap ns1  service method-protocol:	findPacket SOAP
//gsoap ns1  service method-style:	findPacket rpc
//gsoap ns1  service method-encoding:	findPacket literal
//gsoap ns1  service method-input-action:	findPacket http://ws/PacketWS/findPacketRequest
//gsoap ns1  service method-output-action:	findPacket http://ws/PacketWS/findPacketResponse
int ns1__findPacket(
    int                                 arg0,	///< Input parameter
    struct ns1__findPacketResponse     &	///< Output response struct parameter
);

/******************************************************************************\
 *                                                                            *
 * Service Operation                                                          *
 *   ns1__updateTracking                                                      *
 *                                                                            *
\******************************************************************************/

/// Operation response struct "ns1__updateTrackingResponse" of operation "ns1__updateTracking".
struct ns1__updateTrackingResponse
{
    std::string                         _return_;	///< Output parameter, _ wildcard name as per RPC parameterOrder
};

/** Operation "ns1__updateTracking" of service binding "ImplPacketWSPortBinding".

  - SOAP literal messaging

  - Default endpoints:
    - http://localhost:9000/packet

  - Addressing input action: "http://ws/PacketWS/updateTrackingRequest"

  - Addressing output action: "http://ws/PacketWS/updateTrackingResponse"

C stub function (defined in soapClient.c[pp] generated by soapcpp2):
@code
  int soap_call_ns1__updateTracking(
    struct soap *soap,
    NULL, // char *endpoint = NULL selects default endpoint for this operation
    NULL, // char *action = NULL selects default action for this operation
    // input parameters:
    int                                 arg0,
    bool                                arg1,
    // output parameters:
    struct ns1__updateTrackingResponse&
  );
@endcode

C server function (called from the service dispatcher defined in soapServer.c[pp]):
@code
  int ns1__updateTracking(
    struct soap *soap,
    // input parameters:
    int                                 arg0,
    bool                                arg1,
    // output parameters:
    struct ns1__updateTrackingResponse&
  );
@endcode

C++ proxy class (defined in soapImplPacketWSPortBindingProxy.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingProxy;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use proxy classes;

C++ service class (defined in soapImplPacketWSPortBindingService.h generated with soapcpp2):
@code
  class ImplPacketWSPortBindingService;
@endcode
Important: use soapcpp2 option '-j' (or '-i') to generate improved and easy-to-use service classes;

*/

//gsoap ns1  service method-protocol:	updateTracking SOAP
//gsoap ns1  service method-style:	updateTracking rpc
//gsoap ns1  service method-encoding:	updateTracking literal
//gsoap ns1  service method-input-action:	updateTracking http://ws/PacketWS/updateTrackingRequest
//gsoap ns1  service method-output-action:	updateTracking http://ws/PacketWS/updateTrackingResponse
int ns1__updateTracking(
    int                                 _arg0,	///< Input parameter, _ wildcard name as per RPC parameterOrder
    bool                                _arg1,	///< Input parameter, _ wildcard name as per RPC parameterOrder
    struct ns1__updateTrackingResponse &	///< Output response struct parameter
);

/**

@page ImplPacketWSPortBinding Binding "ImplPacketWSPortBinding"

@section ImplPacketWSPortBinding_policy_enablers Policy Enablers of Binding "ImplPacketWSPortBinding"

None specified.

*/

/******************************************************************************\
 *                                                                            *
 * XML Data Binding                                                           *
 *                                                                            *
\******************************************************************************/


/**

@page page_XMLDataBinding XML Data Binding

SOAP/XML services use data bindings contractually bound by WSDL and auto-
generated by wsdl2h and soapcpp2 (see Service Bindings). Plain data bindings
are adopted from XML schemas as part of the WSDL types section or when running
wsdl2h on a set of schemas to produce non-SOAP-based XML data bindings.

The following readers and writers are C/C++ data type (de)serializers auto-
generated by wsdl2h and soapcpp2. Run soapcpp2 on this file to generate the
(de)serialization code, which is stored in soapC.c[pp]. Include "soapH.h" in
your code to import these data type and function declarations. Only use the
soapcpp2-generated files in your project build. Do not include the wsdl2h-
generated .h file in your code.

Data can be read and deserialized from:
  - an int file descriptor, using soap->recvfd = fd
  - a socket, using soap->socket = (int)...
  - a C++ stream (istream, stringstream), using soap->is = (istream*)...
  - a C string, using soap->is = (const char*)...
  - any input, using the soap->frecv() callback

Data can be serialized and written to:
  - an int file descriptor, using soap->sendfd = (int)...
  - a socket, using soap->socket = (int)...
  - a C++ stream (ostream, stringstream), using soap->os = (ostream*)...
  - a C string, using soap->os = (const char**)...
  - any output, using the soap->fsend() callback

The following options are available for (de)serialization control:
  - soap->encodingStyle = NULL; to remove SOAP 1.1/1.2 encodingStyle
  - soap_mode(soap, SOAP_XML_TREE); XML without id-ref (no cycles!)
  - soap_mode(soap, SOAP_XML_GRAPH); XML with id-ref (including cycles)
  - soap_set_namespaces(soap, struct Namespace *nsmap); to set xmlns bindings


@section default Top-level root elements of schema ""

@section ns1 Top-level root elements of schema "http://ws/"

*/

/* End of packet.h */
