package ctrl;

import gui.Message;
import ws.ImplPacketWSService;
import ws.PacketWS;

/**
 * Created by moro on 1/18/17.
 */
public class UserController {

    public static void listPackets(String username) {
        ImplPacketWSService ips = new ImplPacketWSService();
        PacketWS packWs = ips.getImplPacketWSPort();
        packWs.getPacketsByUser(username);
        Message.show("got packets for user " + username);
    }
}
