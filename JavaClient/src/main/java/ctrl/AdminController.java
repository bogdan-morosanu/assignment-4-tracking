package ctrl;

import ws.*;

import java.util.IllegalFormatCodePointException;

/**
 * Created by moro on 1/18/17.
 */
public class AdminController {

    public static void addRoute(String from, String to, String time) {
        ImplRouteWSService irs = new ImplRouteWSService();
        RouteWS routeWs = irs.getImplRouteWSPort();

        Route r = new Route();
        r.setPackageId(Integer.parseInt(from));
        r.setCityTo(to);
        r.setTime(time);

        routeWs.addRoute(r);

    }

    public static void addPackage(String cityFrom,
                                  String cityTo,
                                  String name,
                                  String receiver,
                                  String sender) {

        ImplPacketWSService ips = new ImplPacketWSService();
        PacketWS packWs = ips.getImplPacketWSPort();

        Packet p = new Packet();
        p.setCityFrom(cityFrom);
        p.setCityTo(cityTo);
        p.setName(name);
        p.setReceiver(receiver);
        p.setSender(sender);

        packWs.addPacket(p);
    }

    public static void editTracking(String idval, String tracked) {
            if (tracked.equalsIgnoreCase("true")) {
                int id = Integer.parseInt(idval);
                ImplPacketWSService ips = new ImplPacketWSService();
                PacketWS p = ips.getImplPacketWSPort();
                p.updateTracking(id, true);
            } else {
                int id = Integer.parseInt(idval);
                ImplPacketWSService ips = new ImplPacketWSService();
                PacketWS p = ips.getImplPacketWSPort();
                p.updateTracking(id, false);
            }

    }

    public static void deletePackage(String text) {
        ImplPacketWSService ips = new ImplPacketWSService();
        PacketWS p = ips.getImplPacketWSPort();
        p.deletePacket(Integer.parseInt(text));
    }
}
