package ctrl;

import gui.AdminView;
import gui.Message;
import gui.UserView;
import ws.ImplUserWSService;
import ws.User;
import ws.UserWS;

/**
 * Created by moro on 1/18/17.
 */

public class LoginController {

    public static void doLogin(String username, String pass) {
        ImplUserWSService service = new ImplUserWSService();
        UserWS userws = service.getImplUserWSPort();
        User res = userws.findUser(username);

        if (res != null && res.getPassword().equals(pass)) {

            if (res.getType().equalsIgnoreCase("admin")) {
                AdminView av = new AdminView();
                av.setVisible(true);
            } else {
                UserView uv = new UserView(res);
                uv.setVisible(true);
            }

        } else {

            Message.show("login fail!");
        }
    }

}
