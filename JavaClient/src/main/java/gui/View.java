package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by moro on 11/23/16.
 */
public class View  extends JFrame {

    private JTextField engVal;
    private JTextField priceVal;
    private JTextField yearVal;

    private JButton priceJButton;
    private JTextArea priceRes;

    private JButton taxJButton;
    private JTextArea taxRes;

    public View() {
        setTitle("Cars Client");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);


        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(5, 2));

        JLabel yearLabel = new JLabel("Year");
        ctPane.add(yearLabel);

        yearVal = new JTextField();
        ctPane.add(yearVal);

        JLabel engLabel = new JLabel("Engine Cap.");
        ctPane.add(engLabel);
        engVal = new JTextField();
        ctPane.add(engVal);

        JLabel pchsPriceLabel = new JLabel("Purchased Price");
        ctPane.add(pchsPriceLabel);
        priceVal = new JTextField();
        ctPane.add(priceVal);

        priceJButton = new JButton("get price");
        ctPane.add(priceJButton);
        priceRes = new JTextArea();
        priceRes.setEditable(false);
        ctPane.add(priceRes);

        taxJButton = new JButton("get tax");
        ctPane.add(taxJButton);
        taxRes = new JTextArea();
        taxRes.setEditable(false);
        ctPane.add(taxRes);
    }

    public void addPriceActionLstn(ActionListener al) {
        priceJButton.addActionListener(al);
    }

    public void addTaxActionLstn(ActionListener al) {
        taxJButton.addActionListener(al);
    }

    public int year() throws NumberFormatException {
        return Integer.parseInt(this.yearVal.getText());
    }

    public double pchsPrice() throws NumberFormatException {
        return Double.parseDouble(this.priceVal.getText());
    }

    public double engineCap() throws NumberFormatException {
        return Double.parseDouble(this.engVal.getText());
    }

    public void reset() {
        this.priceRes.setText("");
        this.taxRes.setText("");
    }

    public void updatePrice(double price) {
        this.priceRes.setText(Double.toString(price));
    }

    public void updateTax(double tax) {
        this.taxRes.setText(Double.toString(tax));
    }
}
