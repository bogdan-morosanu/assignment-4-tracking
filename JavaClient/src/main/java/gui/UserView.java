package gui;

import ctrl.UserController;
import ws.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by moro on 1/18/17.
 */
public class UserView extends JFrame {

    final User u;

    public UserView(final User u) {
        this.u = u;

        setTitle("User Pannel");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);

        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(3, 1));

        JButton addRouteBtn = new JButton("see packets");
        ctPane.add(addRouteBtn);

        addRouteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                UserController.listPackets(u.getUsername());
            }
        });

    }

}
