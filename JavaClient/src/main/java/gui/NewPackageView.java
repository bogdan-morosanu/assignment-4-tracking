package gui;

import ctrl.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by moro on 1/18/17.
 */

public class NewPackageView extends JFrame {

    private JTextField cityFrom;
    private JTextField cityTo;
    private JTextField name;
    private JTextField receiver;
    private JTextField sender;

    public NewPackageView() {
        setTitle("New Package");

        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(300, 200);

        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(6, 2));

        JLabel cfromLbl = new JLabel("city from");
        ctPane.add(cfromLbl);
        cityFrom = new JTextField();
        ctPane.add(cityFrom);

        JLabel ctoLbl = new JLabel("city to");
        ctPane.add(ctoLbl);
        cityTo = new JTextField();
        ctPane.add(cityTo);

        JLabel namebl = new JLabel("name");
        ctPane.add(namebl);
        name =  new JTextField();
        ctPane.add(name);

        JLabel recLbl = new JLabel("receiver");
        ctPane.add(recLbl);
        receiver =  new JTextField();
        ctPane.add(receiver);

        JLabel sndLbl = new JLabel("sender");
        ctPane.add(sndLbl);
        sender =  new JTextField();
        ctPane.add(sender);


        JButton add = new JButton("add");
        ctPane.add(add);
        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                AdminController.addPackage(
                        cityFrom.getText(),
                        cityTo.getText(),
                        name.getText(),
                        receiver.getText(),
                        sender.getText()
                );
            }
        });
    }
}
