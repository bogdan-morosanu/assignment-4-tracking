package gui;

import ctrl.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by moro on 1/18/17.
 */

public class NewRouteView extends JFrame {

    private JTextField cityFromVal;
    private JTextField cityToVal;
    private JTextField timeVal;

    public NewRouteView() {
        setTitle("New Route");

        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(300, 200);

        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(4, 3));

        cityFromVal = new JTextField();
        cityToVal = new JTextField();
        timeVal = new JTextField();

        ctPane.add(new JLabel("package id"));
        ctPane.add(cityFromVal);

        ctPane.add(new JLabel("city to"));
        ctPane.add(cityToVal);

        ctPane.add(new JLabel("time"));
        ctPane.add(timeVal);

        JButton add = new JButton("add");
        ctPane.add(add);

        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                AdminController.addRoute(cityFromVal.getText(), cityToVal.getText(), timeVal.getText());
            }
        });
    }
}
