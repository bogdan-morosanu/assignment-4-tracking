package gui;

import ctrl.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by moro on 1/18/17.
 */
public class EditPackageView extends JFrame {

    private JTextField pkgId;
    private JTextField tracked;

    public EditPackageView() {
        setTitle("Edit Package");

        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(300, 200);

        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(6, 2));

        JLabel cfromLbl = new JLabel("id");
        ctPane.add(cfromLbl);
        pkgId = new JTextField();
        ctPane.add(pkgId);

        JLabel ctoLbl = new JLabel("tracked");
        ctPane.add(ctoLbl);
        tracked = new JTextField();
        ctPane.add(tracked);

        JButton add = new JButton("update");
        ctPane.add(add);
        add.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                AdminController.editTracking(
                        pkgId.getText(),
                        tracked.getText()
                );
            }
        });
    }
}
