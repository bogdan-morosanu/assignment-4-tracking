package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by moro on 1/18/17.
 */


public class Message extends JFrame {

    public Message(String msg) {

        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(100, 200);
        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(1, 1));
        JLabel msgLabel = new JLabel(msg);
        ctPane.add(msgLabel);
    }

    public static void show(String msg) {
        Message m = new Message(msg);
        m.setVisible(true);
    }
}
