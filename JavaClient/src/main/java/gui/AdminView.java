package gui;

import ctrl.AdminController;
import ctrl.LoginController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by moro on 1/18/17.
 */
public class AdminView extends JFrame {

    public AdminView() {
        setTitle("Admin Pannel");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);

        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(3, 1));

        JButton addRouteBtn = new JButton("add route");
        ctPane.add(addRouteBtn);

        addRouteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JFrame newRoute = new NewRouteView();
                newRoute.setVisible(true);
            }
        });

        JButton addPkgBtn = new JButton("add package");
        ctPane.add(addPkgBtn);

        addPkgBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JFrame newPack = new NewPackageView();
                newPack.setVisible(true);
            }
        });


        JButton toggleBtn = new JButton("update tracking");
        ctPane.add(toggleBtn);

        toggleBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JFrame newPack = new EditPackageView();
                newPack.setVisible(true);
            }
        });

        JButton delBtn = new JButton("delete package");
        ctPane.add(delBtn);

        delBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JFrame newPack = new DeletePackageView();
                newPack.setVisible(true);
            }
        });
    }
}
