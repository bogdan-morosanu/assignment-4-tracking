package gui;


import ctrl.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by moro on 1/18/17.
 */
public class DeletePackageView extends JFrame {

    private JTextField id;

    public DeletePackageView() {
        setTitle("Delete Package");

        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(300, 200);

        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(3, 1));

        ctPane.add(new JLabel("id"));
        id = new JTextField();
        ctPane.add(id);

        JButton del = new JButton("delete");
        del.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AdminController.deletePackage(id.getText());
            }
        });
        ctPane.add(del);
    }
}















