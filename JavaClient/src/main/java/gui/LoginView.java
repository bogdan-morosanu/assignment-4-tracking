package gui;

import ctrl.LoginController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by moro on 1/18/17.
 */
public class LoginView extends JFrame {

    private JTextField usernameVal;

    private JTextField passwdVal;

    private JButton loginBtn;

    public LoginView() {
        setTitle("Login");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);

        JPanel ctPane = new JPanel();
        ctPane.setBorder(new EmptyBorder(1, 1, 1, 1));
        setContentPane(ctPane);
        ctPane.setLayout(new GridLayout(5, 1));


        JLabel usernameLabel = new JLabel("Username");
        ctPane.add(usernameLabel);

        usernameVal = new JTextField();
        ctPane.add(usernameVal);

        JLabel passwdLabel = new JLabel("Passwd");
        ctPane.add(passwdLabel);

        passwdVal = new JTextField();
        ctPane.add(passwdVal);

        loginBtn = new JButton("login");
        ctPane.add(loginBtn);

        loginBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                LoginController.doLogin(usernameVal.getText(), passwdVal.getText());
            }
        });

    }
}
















